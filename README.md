# Fast training of effective multi-class boosting using coordinate descent optimization

Code author: Guosheng Lin (guosheng.lin@gmail.com or guosheng.lin@adelaide.edu.au)

Correspondence should be addressed to Chunhua Shen (chhshen@gmail.com)


## This is the implementation of the following papers. If you use this code in your research, please cite:

```
@inproceedings{Lin2012,
  author     = {Lin, Guosheng and Shen, Chunhua and van den Hengel, Anton and Suter, David},
  title      = {Fast training of effective multi-class boosting using coordinate descent optimization},
  booktitle  = {Proc. Asian conference on Computer Vision},
  pages      = {782--795},
  url        = {http://dx.doi.org/10.1007/978-3-642-37444-9_61},
  year       = {2012},
}
```
Or

```
@inproceedings{Shen2011CVPRa,
   author    = {Shen, Chunhua  and  Hao, Zhihui},
   title     = {A direct formulation for totally-corrective multi-class boosting},
   booktitle = {Proc. IEEE Conference on Computer Vision and Pattern Recognition},
   pages     = {2585--2592},
   address   = {Colorado Springs, USA},
   url       = {http://hdl.handle.net/2440/62919},
   year      = {2011},
 }
```

We use some code from [Piotr's Image & Video Matlab Toolbox](http://vision.ucsd.edu/~pdollar/toolbox/doc/)
for the fast decision tree implementation, described in this paper:
```
@inproceedings{AppelICML13quickBoost,
  author={Ron Appel and Thomas Fuchs and Piotr Doll\'ar and Pietro Perona},
  title={Quickly Boosting Decision Trees - Pruning Underachieving Features Early},
  booktitle={Prof. Int. Conf. Machine Learning},
  year={2013},
}
```
This code from Piotr's Image & Video Matlab Toolbox is licensed under the Simplified BSD License.



## Installation:

For convenience, the required code of Piotr's Toolbox is included in the folder: piotr_toolbox, and pre-compiled in Linux and OSX. If you use the original Piotr's Toolbox, you need to rename folder: piotr_toolbox/classify/private -> piotr_toolbox/classify/private_tmp.

Run the following in MATLAB:

compile the code from Piotr's Image & Video Matlab Toolbox.

```
cd piotr_toolbox/
run ./external/toolboxCompile.m
```

compile the mex implementation of our method.
```
cd multiboost_cw/
make
```

The file demo.m shows that how to use the code.


## Copyright

Copyright (c) Guosheng Lin, Chunhua Shen. 2013.

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.




