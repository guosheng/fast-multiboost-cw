


function hfeat=apply_wl_tree(feat_data, wlearners)

wl_num=length(wlearners);
e_num=size(feat_data,1);


hfeat=zeros(e_num, wl_num, 'int8');
for wl_idx=1:wl_num

    one_model=wlearners{wl_idx};
    if isempty(one_model)
        one_hfeat=ones(e_num, 1);
    else
        one_hfeat = binaryTreeApply( feat_data, one_model);
        one_hfeat= nonzerosign(one_hfeat);
    end
    hfeat(:, wl_idx)=one_hfeat;
    

end



end
